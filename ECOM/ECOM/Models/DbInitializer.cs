﻿using ECOM.Areas.Identity.Data;
using ECOM.Data;
using Microsoft.AspNetCore.Identity;

namespace ECOM.Models
{
    public static class DbInitializer
    {
        public static async Task InitializeAsync(ECOMContext context, IServiceProvider serviceProvider, UserManager<ECOMUser> userManager) 
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            string[] Rolenames = { "Admin", "User" };
            IdentityResult roleResult;
            foreach (var rolename in Rolenames) 
            {
                var role = await RoleManager.RoleExistsAsync(rolename);
                if (!role) 
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(rolename));
                }
            }
            string Email = "admin@mysite.com";
            string Password = "Admin.23$w";

            if (userManager.FindByEmailAsync(Email).Result == null) 
            {
                ECOMUser user = new ECOMUser();
                user.UserName = Email;
                user.Email = Email;
                IdentityResult result = userManager.CreateAsync(user,Password).Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        
        }
    }
}
