﻿using System.ComponentModel.DataAnnotations;

namespace ECOM.Models
{
    public class BookStore
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
